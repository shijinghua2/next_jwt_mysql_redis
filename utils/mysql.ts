const mysql = require('mysql');

const pool = mysql.createPool({  
    multipleStatements: true,     // 允许执行多条查询语句
    host     : process.env.DB_HOST, 
    port     : process.env.DB_PORT,      
    user     : process.env.DB_USER,              
    password : process.env.DB_PASS,                         
    database : process.env.DB_DATABASE
});

export default pool;