import jsonwebtoken from 'jsonwebtoken';
const secret = 'test_key111';

export default class JWT {
    // 生成令牌token
    public static generate(value: any, expires: string = '1 days'): string {
        try {
            return jsonwebtoken.sign(value, secret, { expiresIn: expires });
        } catch (e) {
            console.error('jwt sign error --->', e);
            return '';
        }
    }
    // 验证token
    public static verify(token: string) {
        try {
            return jsonwebtoken.verify(token, secret);     // 如果过期会返回false
        } catch (e) {
            console.error('jwt verify error --->', e);
            return false;
        }
    }
}