import type { NextApiRequest, NextApiResponse } from 'next'
import JWT from '../../utils/jwt'
import pool from '../../utils/mysql'
import client from '../../utils/redis'
import { getCookie } from "../../utils/cookie"

type Data = {
  code: number,
  data: any,
  msg: string
}

enum ReqType {
    login = 'login',
    regist = 'regist',
    changepwd = 'changepwd'
}

function loginSQL(res: NextApiResponse<Data>, username: string, password: string) {
    pool.getConnection((err: any, conn: any) => {
        if (err) {
          console.log('数据库连接失败')
          res.status(500).end();
        } else {
          const searchSQL = `SELECT * FROM user_info WHERE username='${username}' AND password='${password}'`;
          conn.query(searchSQL, (err: any, result: any) => {
            if (err) {      // 查询失败
              console.log('数据查询失败')
              res.status(500).end();
            } else {        // 查询成功
                if (result.length === 0) {
                    res.status(200).json({ code: -1, data: { }, msg: '用户名或密码错误' });
                } else {
                    // 账号密码验证成功后生成一个新的token发送给客户端
                    const token = JWT.generate({ username, password }, '60s');
                    res.status(200).json({ code: 0, data: { token }, msg: '登录成功！' });
                }
            }
            conn.release();
          })
        }
    })
}

function registSQL(res: NextApiResponse<Data>, username: string, password: string) {
    pool.getConnection((err: any, conn: any) => {
        if (err) {
          res.status(500).end();
        } else {
          const info = { username, password };
          const addSQL = "INSERT INTO user_info SET ?";
          conn.query(addSQL, info, (err: any) => {
            if (err) {      // 插入失败
              console.log('插入信息失败',err);
              res.status(500).json({ code: -1, data: { }, msg: '用户名已被注册！' });
            } else {        // 插入成功
              // 账号密码验证成功后生成一个新的token发送给客户端
              const token = JWT.generate({ username, password }, '5min');
              res.status(200).json({ code: 0, data: { token }, msg: '注册成功！' });
            }
            conn.release();
          })
        }
    })
}

function changepwdSQL(req: NextApiRequest, res: NextApiResponse<Data>, username: string, password: string, passwordnew: string) {
    pool.getConnection((err: any, conn: any) => {
        if (err) {
            res.status(500).end();
        } else {
            const searchSQL = `SELECT * FROM user_info WHERE username='${username}' AND password='${password}'`;
            conn.query(searchSQL, (err: any, result: any[]) => {
            if (err) {      // 查询失败
                res.status(500).end();
            } else {        // 查询成功
                if (result.length === 0) {
                    res.status(200).json({ code: -1, data: { }, msg: '用户不存在' });
                } else {
                    // 修改密码
                    const updateSQL = `UPDATE user_info SET password='${passwordnew}' WHERE username='${username}'`;
                    conn.query(updateSQL, (err: any) => {
                        if (err) {
                            res.status(500).end();
                        } else {
                            // 拿到原token加入redis黑名单
                            const oldtoken = getCookie('token', req) || '';
                            client.set(username, oldtoken);
                            // 账号和新密码验证成功后生成一个新的token发送给客户端
                            const Newtoken = JWT.generate({ username, password: passwordnew }, '60s');
                            res.status(200).json({ code: 0, data: { token: Newtoken }, msg: '修改成功！' });
                        }
                    })
                }
            }
            conn.release();
            })
        }
    })
}

export default function handle(req: NextApiRequest, res: NextApiResponse<Data>) {
    const { type } = req.query;
    if (type === ReqType.login || type === ReqType.regist) {
        if(req.method === 'POST') {
            const { username, password } = req.body;
            if (!username || !password) {
              res.status(500).json({ code: -1, data: { }, msg: '用户名或密码不能为空' });
              return;
            }
            type === ReqType.login ? loginSQL(res, username, password) : registSQL(res, username, password)
        }
    } else if (type === ReqType.changepwd) {
        if(req.method === 'POST') {
            const { username, password, passwordnew } = req.body;
            if (!username || !password || !passwordnew) {
                res.status(500).json({ code: -1, data: { }, msg: '用户名或密码不能为空' });
                return;
            }
            changepwdSQL(req, res, username, password, passwordnew)
        }
    }
}