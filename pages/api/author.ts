import type { NextApiRequest, NextApiResponse } from 'next'
import JWT from '../../utils/jwt'
import client from '../../utils/redis'

type Data = {
  code: number,
  data: any,
  msg: string
}

export default function authorize(req: NextApiRequest, res: NextApiResponse<Data>) {
    const { authorization } = req.headers;
    console.log(77777777777777, authorization);
    const token = authorization?.replace('Bearer ', '') || '';
    console.log(88888888888, token);
    if (!token) {
        res.status(200).json({ code: -1, data: {}, msg: '用户未登录' })
    } else {
        const isValid = JWT.verify(token)
        if (!isValid) {
            res.status(200).json({ code: -1, data: {}, msg: '用户登录信息已过期' })
        } else { 
            const username = typeof isValid !== 'string' ? isValid.username : '';
            client.get(username, (err: any, v: string) => {
                console.log('redis', v)
                if (token === v) {   // 如果在redis黑名单中，表明jwt失效
                    res.status(200).json({ code: -1, data: {}, msg: '用户登录信息已过期' })
                } else {
                    res.status(200).json({ code: 0, data: { username }, msg: '用户已登录' })
                }
            })
        }
    }
}