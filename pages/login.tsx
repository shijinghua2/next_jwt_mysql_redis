import styles from '../styles/Form.module.scss'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { setCookie } from '../utils/cookie'
const crypto = require('crypto')

export default function Login(){
    const [userInfo, setUserInfo] = useState({username: '', password: ''})
    const router = useRouter()
    const loginFn = async (e: any) => {
        e.preventDefault();
        const { username, password } = userInfo;
        const obsPassword = crypto.createHash('sha1').update(password).digest('hex')
        const res = await fetch(`http://${process.env.NEXT_PUBLIC_HOST}:${process.env.NEXT_PUBLIC_PORT}/api/login`,{
            method: 'POST',
            body: JSON.stringify({ username, password: obsPassword }),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        const json = await res.json()
        // 登录成功，存储拿到的token
        if (json.code === 0) {
            const { token } = json.data;
            setCookie("token", token);
            router.push('/')
        } else {
            alert(json.msg || '登录失败请重试！')
        }
    }
    return (
        <div className={styles.container}>
            <form onSubmit={loginFn}>
                <h2>登录</h2>
                <div className={styles.list}>
                    <label>账号：</label>
                    <input name="username" type="text" placeholder="请输入账号" required value={userInfo.username} onChange={(e)=>{setUserInfo({...userInfo, username: e.target.value})}}/>
                </div>
                <div className={styles.list}>
                    <label>密码：</label>
                    <input name="password" type="password" placeholder="请输入密码" required value={userInfo.password} onChange={(e)=>{setUserInfo({...userInfo, password: e.target.value})}}/>
                </div>
                <button type="submit">登录</button>
                <Link href="/">
                    <button>返回</button>
                </Link>
            </form>
        </div>
    )
}