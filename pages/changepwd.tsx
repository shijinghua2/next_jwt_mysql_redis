import styles from '../styles/Form.module.scss'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { setCookie } from '../utils/cookie'
const crypto = require('crypto')

export default function ChangePWD() {
    const [userInfo, setUserInfo] = useState({username: '', password: '', passwordnew: ''})
    const router = useRouter()
    const changePwdFn = async (e: any) => {
        e.preventDefault();
        const { username, password, passwordnew } = userInfo
        const obsPassword = crypto.createHash('sha1').update(password).digest('hex')
        const obsPasswordNew = crypto.createHash('sha1').update(passwordnew).digest('hex')
        const res = await fetch(`http://${process.env.NEXT_PUBLIC_HOST}:${process.env.NEXT_PUBLIC_PORT}/api/changepwd`,{
            method: 'POST',
            body: JSON.stringify({ username, password: obsPassword, passwordnew: obsPasswordNew }),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        const json = await res.json();
        // 修改密码成功，存储拿到的新token
        if (json.code === 0) {
            const { token } = json.data;
            setCookie("token", token);
            router.push('/');
        } else {
            alert(json.msg || '修改密码失败！');
        }
    }
    return (
        <div className={styles.container}>
            <form onSubmit={changePwdFn}>
                <h2>修改密码</h2>
                <div className={styles.list}>
                    <label>账号：</label>
                    <input name="username" type="text" placeholder="请输入账号" required value={userInfo.username} onChange={(e)=>{setUserInfo({...userInfo, username: e.target.value})}}/>
                </div>
                <div className={styles.list}>
                    <label>原始密码：</label>
                    <input name="password" type="password" placeholder="请输入原密码" required value={userInfo.password} onChange={(e)=>{setUserInfo({...userInfo, password: e.target.value})}}/>
                </div>
                <div className={styles.list}>
                    <label>新密码：</label>
                    <input name="passwordnew" type="password" placeholder="请输入新密码" required value={userInfo.passwordnew} onChange={(e)=>{setUserInfo({...userInfo, passwordnew: e.target.value})}}/>
                </div>
                <button type="submit">确定</button>
                <Link href="/">
                    <button>返回</button>
                </Link>
            </form>
        </div>
    )
}