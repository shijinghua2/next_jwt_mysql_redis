import Link from 'next/link'
import { getCookie, removeCookie } from "../utils/cookie"
import { useRouter } from 'next/router'
import styles from '../styles/Home.module.scss'

interface IProps {
  username : string
}

export default function Home(props: IProps) {
  const router = useRouter()
  const { username } = props
  // 注销账户？todo

  // 退出登录
  const logoutFn = () => {
    removeCookie('token');
    router.push('/login');
  }
  // 修改密码
  const changePWD = () => {
    router.push('/changepwd');
  }
  return (
    <div className={styles.container}>
    { !username ? (
      <>
        <h3>
          新用户？赶紧去
          <Link href="/register">
            <a className={styles.link}>注册</a>
          </Link>
          一个吧！
        </h3>
        <h3>
          已有账号？去
          <Link href="/login">
            <a className={styles.link}>登录</a>
          </Link>
        </h3>
      </>
    ) : (
      <>
        <h3>
          欢迎<span className={styles.username}>{username}</span>！
        </h3>
        <button onClick={logoutFn}>退出登录</button>
        <button onClick={changePWD}>修改密码</button>
      </>
    )}
    </div>
  )
}

export async function getServerSideProps(ctx: any){
  // 获取存储的token （服务端渲染没法去从localStorage拿）
  const token = getCookie('token', ctx.req) || '';
  // 将token放到header里面
  const res = await fetch(`http://${process.env.NEXT_PUBLIC_HOST}:${process.env.NEXT_PUBLIC_PORT}/api/author`,{
    method: 'GET',
    headers: new Headers({
      'authorization': 'Bearer ' + token
    })
  })
  const json = await res.json()
  console.log(json)
  return {
    props: json.data
  }
}



// 在build阶段返回页面所需的数据
// export const getStaticProps: GetStaticProps = async (context) => {

// }


// 如果是动态路由的页面，使用getStaticPaths方法来返回所有的路由参数，以及是否需要回落机制
// export const getStaticPaths: GetStaticPaths = async () => {

// }


// 1.方法只会在服务端运行，每次请求都运行一遍getServerSideProps方法
// 2.如果页面通过浏览器端Link组件导航而来，Next会向服务端发一个请求，然后在服务端运行getServerSideProps方法，然后返回JSON到浏览器
// export const getServerSideProps: GetServerSideProps = async (context) => {
//         const list  = await context.req.service.post.getPost(context.params.postID)
// }