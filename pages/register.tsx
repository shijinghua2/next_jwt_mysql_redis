import styles from '../styles/Form.module.scss'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { setCookie } from '../utils/cookie'
const crypto = require('crypto')

export default function Register() {
    const [userInfo, setUserInfo] = useState({username: '', password: '', passwordcopy: ''})
    const router = useRouter()
    const registFn = async (e: any) => {
        e.preventDefault();
        const { username, password, passwordcopy } = userInfo;
        const pwdRegex = new RegExp('(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^a-zA-Z0-9]).{6,18}');
        if (!pwdRegex.test(password)) {
            alert('密码中必须包含大小写字母、数字、特殊字符,长度至少6位,至多18位');
            return;
        }
        if (password !== passwordcopy) {
            alert('密码不一致，请确认密码填写正确！');
            return;
        }
        const obsPassword = crypto.createHash('sha1').update(password).digest('hex')
        const res = await fetch(`http://${process.env.NEXT_PUBLIC_HOST}:${process.env.NEXT_PUBLIC_PORT}/api/regist`,{
            method: 'POST',
            body: JSON.stringify({ username, password: obsPassword }),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        const json = await res.json()
        // 注册成功，存储拿到的token
        if (json.code === 0) {
            const { token } = json.data;
            setCookie("token", token);
            router.push('/');
        } else {
            alert(json.msg || '注册失败请重试！');
        }
    }
    return (
        <div className={styles.container}>
            <form onSubmit={registFn}>
                <h2>申请注册并登录</h2>
                <div className={styles.list}>
                    <label>账号：</label>
                    <input name="username" type="text" placeholder="请输入账号" required value={userInfo.username} onChange={(e)=>{setUserInfo({...userInfo, username: e.target.value})}}/>
                </div>
                <div className={styles.list}>
                    <label>密码：</label>
                    <input name="password" type="password" placeholder="请输入密码" required value={userInfo.password} onChange={(e)=>{setUserInfo({...userInfo, password: e.target.value})}}/>
                </div>
                <div className={styles.list}>
                    <label>重复密码：</label>
                    <input name="passwordcopy" type="password" placeholder="请重复密码" required value={userInfo.passwordcopy} onChange={(e)=>{setUserInfo({...userInfo, passwordcopy: e.target.value})}}/>
                </div>
                <button type="submit">注册</button>
                <Link href="/">
                    <button>返回</button>
                </Link>
            </form>
        </div>
    )
}